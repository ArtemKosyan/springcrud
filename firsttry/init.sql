-- Создание таблицы "Сотрудники"
CREATE TABLE employees (
    id_employee SERIAL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL,
    phone VARCHAR(20),
    department_id INT NOT NULL,
    position_id INT NOT NULL,
    hire_date DATE NOT NULL,
    salary DECIMAL(10,2) NOT NULL
);

-- Создание таблицы "Отделы"
CREATE TABLE departments (
    id_department SERIAL PRIMARY KEY,
    department_name VARCHAR(100) UNIQUE NOT NULL,
    manager_id INT DEFAULT NULL
);

-- Создание таблицы "Должности"
CREATE TABLE positions (
    id_position SERIAL PRIMARY KEY,
    position_title VARCHAR(100) UNIQUE NOT NULL,
    department_id INT,
    min_salary DECIMAL(10,2) NOT NULL,
    max_salary DECIMAL(10,2) NOT NULL
);

ALTER TABLE employees ADD CONSTRAINT fk_department_id
	FOREIGN KEY (department_id) REFERENCES departments(id_department);
ALTER TABLE employees ADD CONSTRAINT fk_position_id
	FOREIGN KEY (position_id) REFERENCES positions(id_position);

ALTER TABLE departments ADD CONSTRAINT fk_manager_id
	FOREIGN KEY (manager_id) REFERENCES employees(id_employee) ON DELETE SET DEFAULT;

ALTER TABLE positions ADD CONSTRAINT fk_department_id
	FOREIGN KEY (department_id) REFERENCES departments(id_department);
	
	
CREATE OR REPLACE FUNCTION check_salary_bounds()
RETURNS TRIGGER AS $$
    DECLARE min_salary_bound DECIMAL(10,2);
    DECLARE max_salary_bound DECIMAL(10,2);
BEGIN
    SELECT max_salary INTO max_salary_bound
    	FROM positions
    	WHERE id_position = NEW.position_id;
	
	SELECT min_salary INTO min_salary_bound
    	FROM positions
    	WHERE id_position = NEW.position_id;
    
    IF NEW.salary < min_salary_bound OR NEW.salary > max_salary_bound THEN
        RAISE EXCEPTION 'Salary is outside the bounds for the position of employee';
    END IF;
    
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER enforce_salary_bounds
BEFORE INSERT OR UPDATE ON employees
FOR EACH ROW EXECUTE FUNCTION check_salary_bounds();

CREATE OR REPLACE FUNCTION check_manager_employee_relationship()
RETURNS TRIGGER AS $$
BEGIN
    IF NEW.manager_id IS NOT NULL THEN
        IF NOT EXISTS (SELECT 1 FROM employees WHERE id_employee = NEW.manager_id AND department_id = NEW.id_department) THEN
            RAISE EXCEPTION 'Назначенный менеджер не является сотрудником данного отдела';
        END IF;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER manager_employee_relationship_check
BEFORE UPDATE OR INSERT ON departments
FOR EACH ROW
EXECUTE FUNCTION check_manager_employee_relationship();

-- Отключение авто присваивания поля id_department
ALTER SEQUENCE departments_id_department_seq OWNED BY NONE;
-- Заполнение таблицы "Отделы"
INSERT INTO departments (id_department, department_name) VALUES
(1, 'Sales'),
(2, 'Marketing'),
(3, 'IT'),
(4, 'HR'),
(5, 'Finance'),
(6, 'Operations'),
(7, 'Customer Service'),
(8, 'Research and Development'),
(9, 'Quality Assurance'),
(10, 'Data Analysis'),
(11, 'Business Development'),
(12, 'Product Management'),
(13, 'Software Engineering'),
(14, 'Network Administration'),
(15, 'Database Administration'),
(16, 'Security'),
(17, 'IT Support'),
(18, 'Web Development'),
(19, 'Mobile Development'),
(20, 'Artificial Intelligence');
-- Восстановление привязки последовательности
SELECT setval('departments_id_department_seq', (SELECT MAX(id_department) FROM departments) + 1);
ALTER SEQUENCE departments_id_department_seq OWNED BY departments.id_department;
-- ALTER SEQUENCE departments_id_department_seq OWNED BY departments.id_department;

-- Заполнение таблицы "Должности"
ALTER SEQUENCE positions_id_position_seq OWNED BY NONE;
INSERT INTO positions (id_position, position_title, department_id, min_salary, max_salary) VALUES
(1, 'Sales Manager', 1, 50000.00, 100000.00),
(2, 'Marketing Manager', 2, 60000.00, 120000.00),
(3, 'IT Manager', 3, 70000.00, 140000.00),
(4, 'HR Manager', 4, 80000.00, 160000.00),
(5, 'Financial Analyst', 5, 90000.00, 180000.00),
(6, 'Operations Manager', 6, 100000.00, 200000.00),
(7, 'Customer Service Representative', 7, 40000.00, 80000.00),
(8, 'Research Scientist', 8, 50000.00, 100000.00),
(9, 'Quality Assurance Engineer', 9, 60000.00, 120000.00),
(10, 'Data Analyst', 10, 70000.00, 140000.00),
(11, 'Business Development Manager', 11, 80000.00, 160000.00),
(12, 'Product Manager', 12, 90000.00, 180000.00),
(13, 'Software Engineer', 13, 100000.00, 200000.00),
(14, 'Network Administrator', 14, 100000.00, 200000.00),
(15, 'Database Administrator', 15, 110000.00, 220000.00),
(16, 'Security Specialist', 16, 120000.00, 240000.00),
(17, 'IT Support Specialist', 17, 130000.00, 260000.00),
(18, 'Web Developer', 18, 140000.00, 280000.00),
(19, 'Mobile Developer', 19, 150000.00, 300000.00),
(20, 'Artificial Intelligence Engineer', 20, 160000.00, 320000.00);
-- Восстановление привязки последовательности
SELECT setval('positions_id_position_seq', (SELECT MAX(id_position) FROM positions) + 1);
ALTER SEQUENCE positions_id_position_seq OWNED BY positions.id_position;
-- ALTER SEQUENCE positions_id_position_seq OWNED BY positions.id_position;

-- Заполнение таблицы "Сотрудники"
ALTER SEQUENCE employees_id_employee_seq OWNED BY NONE;
INSERT INTO employees (id_employee, first_name, last_name, email, phone, department_id, position_id, hire_date, salary)
VALUES
(1, 'John', 'Doe', 'john.doe@example.com', '+7(555)-456-78-90', 1, 1, '2020-01-01', 50000.00),
(2, 'Jane', 'Smith', 'jane.smith@example.com', '+7(555)-654-32-10', 2, 2, '2019-06-01', 60000.00);
-- Восстановление привязки последовательности
SELECT setval('employees_id_employee_seq', (SELECT MAX(id_employee) FROM employees) + 1);
ALTER SEQUENCE employees_id_employee_seq OWNED BY employees.id_employee;
-- ALTER SEQUENCE employees_id_employee_seq OWNED BY employees.id_employee;

-- Вставка оставшихся данных
INSERT INTO employees (first_name, last_name, email, phone, department_id, position_id, hire_date, salary)
VALUES
('Bob', 'Johnson', 'bob.johnson@example.com', '+7(555)-123-45-67', 1, 1, '2018-03-01', 55000.00),
('Alice', 'Williams', 'alice.williams@example.com', '+7(555)-555-55-55', 2, 2, '2017-09-01', 65000.00),
('Mike', 'Davis',
'mike.davis@example.com', '+7(555)-555-55-56', 1, 1, '2016-06-01', 50000.00),
('Emily', 'Brown', 'emily.brown@example.com', '+7(555)-555-55-57', 2, 2, '2015-03-01', 60000.00),
('Tom', 'Miller', 'tom.miller@example.com', '+7(555)-555-55-58', 1, 1, '2014-09-01', 55000.00),
('Lily', 'Taylor', 'lily.taylor@example.com', '+7(555)-555-55-59', 2, 2, '2013-06-01', 65000.00),
('Sam', 'Harris', 'sam.harris@example.com', '+7(555)-555-55-60', 1, 1, '2012-03-01', 50000.00),
('Rachel', 'Martin', 'rachel.martin@example.com', '+7(555)-555-55-61', 2, 2, '2011-09-01', 60000.00),
('Kevin', 'Lee', 'kevin.lee@example.com', '+7(555)-555-55-62', 1, 1, '2010-06-01', 55000.00),
('Sarah', 'Hall', 'sarah.hall@example.com', '+7(555)-555-55-63', 2, 2, '2009-03-01', 65000.00),
('James', 'Walker', 'james.walker@example.com', '+7(555)-555-55-64', 1, 1, '2008-09-01', 50000.00),
('Jessica', 'White', 'jessica.white@example.com', '+7(555)-555-55-65', 2, 2, '2007-06-01', 60000.00),
('David', 'Jackson', 'david.jackson@example.com', '+7(555)-555-55-66', 1, 1, '2006-03-01', 55000.00),
('Laura', 'Thompson', 'laura.thompson@example.com', '+7(555)-555-55-67', 2, 2, '2005-09-01', 65000.00),
('Richard', 'Garcia', 'richard.garcia@example.com', '+7(555)-555-55-68', 1, 1, '2004-06-01', 50000.00),
('Katherine', 'Hernandez', 'katherine.hernandez@example.com', '+7(555)-555-55-69', 2, 2, '2003-03-01', 60000.00),
('Michael', 'Patel', 'michael.patel@example.com', '+7(555)-555-55-70', 1, 1, '2002-09-01', 55000.00),
('Elizabeth', 'Kim', 'elizabeth.kim@example.com', '+7(555)-555-55-71', 2, 2, '2001-06-01', 65000.00),
('Matthew', 'Gonzalez', 'matthew.gonzalez@example.com', '+7(555)-555-55-72', 1, 1, '2000-03-01', 50000.00),
('Hannah', 'Nelson', 'hannah.nelson@example.com', '+7(555)-555-55-73', 2, 2, '1999-09-01', 60000.00);

-- Указание менеджеров для отделов
UPDATE departments SET manager_id = 1 WHERE id_department = 1; -- Sales
UPDATE departments SET manager_id = 2 WHERE id_department = 2; -- Marketing
