package myrest.firsttry.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.Repository;

import myrest.firsttry.entities.Department;
import org.springframework.data.repository.CrudRepository;
public interface DepartmentRepository extends CrudRepository<Department, Long> {
    // Iterable<Department> findAll();

    // Department save(Department department);
}
