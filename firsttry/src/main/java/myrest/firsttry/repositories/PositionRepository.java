package myrest.firsttry.repositories;

import myrest.firsttry.entities.Position;

import org.springframework.data.repository.CrudRepository;

public interface PositionRepository extends CrudRepository<Position, Long>{

}
