package myrest.firsttry.web.api;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import myrest.firsttry.entities.Employee;
import myrest.firsttry.entities.Position;
import myrest.firsttry.repositories.DepartmentRepository;
import myrest.firsttry.repositories.EmployeeRepository;
import myrest.firsttry.repositories.PositionRepository;

@RestController
@RequestMapping("/web/api/positions")
public class PositionController {
    private final PositionRepository positionRepository;
    private final DepartmentRepository departmentRepository;

    public PositionController(PositionRepository positionRepository,
                              DepartmentRepository departmentRepository) {
        this.positionRepository = positionRepository;
        this.departmentRepository = departmentRepository;
    }

    @GetMapping
    public Iterable<Position> getAllPositions() {
        return positionRepository.findAll();
    }
    @GetMapping("/{id}")
    public ResponseEntity<Position> getPositionById(@PathVariable Long id) {
        Optional<Position> position = positionRepository.findById(id);
        return position.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/createPosition")
    public ResponseEntity<Position> createPosition(@RequestBody Position position) {
        if (position.getDepartmentId() != null) {
            if (departmentRepository.existsById(position.getDepartmentId())) {
                return ResponseEntity.ok(positionRepository.save(position));
            }
            else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(position);
            }              
        }
        else {
            return ResponseEntity.ok(positionRepository.save(position));
        }   
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePosition(@PathVariable Long id) {
        if (positionRepository.existsById(id)) {
            positionRepository.deleteById(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Position> updateDepartment(@PathVariable Long id, @RequestBody Position updatedPosition) {
        if (positionRepository.existsById(id)) {
            updatedPosition.setIdPosition(id);

            if (updatedPosition.getDepartmentId() != null) {
                if (departmentRepository.existsById(updatedPosition.getDepartmentId())) {
                    return ResponseEntity.ok(positionRepository.save(updatedPosition));
                }
                else {
                    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(updatedPosition);
                }              
            }
            else {
                return ResponseEntity.ok(positionRepository.save(updatedPosition));
            }

        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(updatedPosition);
        }
    }
}