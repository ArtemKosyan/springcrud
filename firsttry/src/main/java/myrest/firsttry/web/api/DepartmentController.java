package myrest.firsttry.web.api;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import myrest.firsttry.entities.Department;
import myrest.firsttry.entities.Employee;
import myrest.firsttry.repositories.DepartmentRepository;
import myrest.firsttry.repositories.EmployeeRepository;

@RestController
@RequestMapping("/web/api/departments")
public class DepartmentController {
    private final DepartmentRepository departmentRepository;
    private final EmployeeRepository employeeRepository;

    public DepartmentController(DepartmentRepository departmentRepository,
        EmployeeRepository employeeRepository) {
        this.departmentRepository = departmentRepository;
        this.employeeRepository = employeeRepository;
    }

    @GetMapping
    public Iterable<Department> getAllDepartments() {
        return departmentRepository.findAll();
    }    
    @GetMapping("/{id}")
    public ResponseEntity<Department> getDepartmentById(@PathVariable Long id) {
        Optional<Department> department = departmentRepository.findById(id);
        return department.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/createDepartment")
    public ResponseEntity<Department> createDepartment(@RequestBody Department department) {
        if (department.getManagerId() != null) {
            if (employeeRepository.existsById(department.getManagerId())) {
                return ResponseEntity.ok(departmentRepository.save(department));
            }
            else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(department);
            }              
        }
        else {
            return ResponseEntity.ok(departmentRepository.save(department));
        }   
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteDepartment(@PathVariable Long id) {
        if (departmentRepository.existsById(id)) {
            departmentRepository.deleteById(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Department> updateDepartment(@PathVariable Long id, @RequestBody Department updatedDepartment) {
        if (departmentRepository.existsById(id)) {
            updatedDepartment.setIdDepartment(id);
            if (updatedDepartment.getManagerId() != null) {

                if (employeeRepository.existsById(updatedDepartment.getManagerId())) {
                    
                    Optional<Employee> wantBeManager = employeeRepository.findById(updatedDepartment.getManagerId());
                    Employee manager = wantBeManager.get();
                    if (manager.getDepartmentId() == updatedDepartment.getIdDepartment()) {
                        return ResponseEntity.ok(departmentRepository.save(updatedDepartment));
                    } else {
                        return ResponseEntity.status(HttpStatus.FAILED_DEPENDENCY).body(updatedDepartment);
                    }                
                }
                else {
                    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(updatedDepartment);
                }    
            } else {
                return ResponseEntity.ok(departmentRepository.save(updatedDepartment));
            }
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(updatedDepartment);
        }
    }
}
