package myrest.firsttry.web.api;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import myrest.firsttry.entities.Department;
import myrest.firsttry.entities.Employee;
import myrest.firsttry.repositories.DepartmentRepository;
import myrest.firsttry.repositories.EmployeeRepository;
import myrest.firsttry.repositories.PositionRepository;

@RestController
@RequestMapping("/web/api/employees")
public class EmployeeController {
    private final EmployeeRepository employeeRepository;
    private final DepartmentRepository departmentRepository;
    private final PositionRepository positionRepository;

    public EmployeeController(EmployeeRepository employeeRepository, 
                              DepartmentRepository departmentRepository,
                              PositionRepository positionRepository) {
        this.employeeRepository = employeeRepository;
        this.departmentRepository = departmentRepository;
        this.positionRepository = positionRepository;
    }

    @GetMapping
    public Iterable<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }
    @GetMapping("/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable Long id) {
        Optional<Employee> employee = employeeRepository.findById(id);
        return employee.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/createEmployee")
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {
        if ((employee.getDepartmentId()!=null)&&(employee.getPositionId()!=null)) {
            if ((positionRepository.existsById(employee.getPositionId())) &&
                (departmentRepository.existsById(employee.getDepartmentId()))) {
                return ResponseEntity.ok(employeeRepository.save(employee));
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(employee);
            }
        } else {
            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(employee);
        }        
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEmployee(@PathVariable Long id) {
        if (employeeRepository.existsById(id)) {
            employeeRepository.deleteById(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @PutMapping("/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable Long id, @RequestBody Employee updatedEmployee) {
        if (employeeRepository.existsById(id)) {
            updatedEmployee.setIdEmployee(id);

            if ((updatedEmployee.getDepartmentId()!=null)&&(updatedEmployee.getPositionId()!=null)) {
                if ((positionRepository.existsById(updatedEmployee.getPositionId())) &&
                    (departmentRepository.existsById(updatedEmployee.getDepartmentId()))) {
                    return ResponseEntity.ok(employeeRepository.save(updatedEmployee));
                } else {
                    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(updatedEmployee);
                }
            } else {
                return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(updatedEmployee);
            }

        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(updatedEmployee);
        }
    }
}
