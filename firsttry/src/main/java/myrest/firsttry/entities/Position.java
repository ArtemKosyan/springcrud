package myrest.firsttry.entities;

import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import lombok.Data;

@Entity
@Table(name = "Positions")
@Data
@AllArgsConstructor
@NoArgsConstructor(access=AccessLevel.PRIVATE, force=true)
public class Position {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_position")
    private Long idPosition;

    @Column(name = "position_title", nullable = false)
    private String positionTitle;

    @Column(name = "department_id", nullable = false)    
    private Long departmentId;

    @Column(name = "min_salary", nullable = false)
    private Double maxSalary;

    @Column(name = "max_salary", nullable = false)
    private Double minSalary;
    // @ManyToOne(optional = false)
    // @JoinColumn(name = "department_id", referencedColumnName = "id_department")
    // private Department department;
}